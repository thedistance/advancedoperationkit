Pod::Spec.new do |s|
  s.name 		 = 'AdvancedOperationKit'
  s.version 	 = '0.5'
  s.license 	 = { :type => 'MIT', :file => 'LICENSE.txt' }
  s.summary 	 = 'Convenience Operations to make the code from Advanced NSOperations more useful.'
  s.description  = 'AdvancedOperationKit wraps the example code given by Apple in the 2015 WWDC developer presentation \'Advanced NSOperations\' into a usable framework.'
  s.homepage     = "https://github.com/thedistance"
  s.license      = "MIT"
  s.author       = { "The Distance" => "dev@thedistance.co.uk" }
  s.source 		 = { :git => 'https://bitbucket.org/thedistance/advancedoperationkit.git', :tag => "#{s.version}" }
  s.swift_version = '4.2'

  s.dependency 'PSOperations'
  s.ios.deployment_target = '8.0'
  
  s.source_files = s.source_files  = "**/AlertOperation.swift", "**/BackgroundObserver.swift", "**/NetworkObserver.swift", "**/CameraCondition.swift", "**/Convenience Operations/*.swift"

  s.exclude_files = "AdvancedOperationKitTests" 

  s.requires_arc = true
end
