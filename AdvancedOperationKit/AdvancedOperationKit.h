//
//  AdvancedOperationKit.h
//  AdvancedOperationKit
//
//  Created by Josh Campion on 04/07/2015.
//  Copyright (c) 2015 Josh Campion. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for AdvancedOperationKit.
FOUNDATION_EXPORT double AdvancedOperationKitVersionNumber;

//! Project version string for AdvancedOperationKit.
FOUNDATION_EXPORT const unsigned char AdvancedOperationKitVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <AdvancedOperationKit/PublicHeader.h>


