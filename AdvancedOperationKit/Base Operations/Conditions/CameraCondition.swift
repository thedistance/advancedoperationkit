//
//  CameraCondition.swift
//  Pods
//
//  Created by Josh Campion on 20/09/2015.
//
//

import Foundation
import PSOperations

#if os(iOS)
    
    import AVKit
    import AVFoundation
    
    /// A condition for verifying access to the user's Photos library.
    struct CameraCondition: OperationCondition {
        
        static let name = "Camera"
        static let isMutuallyExclusive = false
        
        init() { }
        
        func dependencyForOperation(_ operation: PSOperation) -> Foundation.Operation? {
            return CameraPermissionOperation()
        }
        
        func evaluateForOperation(_ operation: PSOperation, completion: @escaping (OperationConditionResult) -> Void) {

          switch AVCaptureDevice.authorizationStatus(for: AVMediaType.video) {
            case .authorized:
                completion(.satisfied)
                
            default:
                let error = NSError(code: .conditionFailed, userInfo: [
                    OperationConditionKey: type(of: self).name
                    ])
                
                completion(.failed(error))
            }
        }
    }
    
    /**
    A private `Operation` that will request access to the user's Camera, if it
    has not already been granted.
    */
    private class CameraPermissionOperation: PSOperation {
        override init() {
            super.init()
            
            addCondition(AlertPresentation())
        }
        
        override func execute() {
          switch AVCaptureDevice.authorizationStatus(for: AVMediaType.video) {
            case .notDetermined:
                DispatchQueue.main.async {
                  AVCaptureDevice.requestAccess(for: AVMediaType.video, completionHandler: { (granted) -> Void in
                        self.finish()
                    })
                }
            default:
                finish()
            }
        }
        
    }
    
#endif


