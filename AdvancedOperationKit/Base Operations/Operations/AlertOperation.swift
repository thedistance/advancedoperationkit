/*
Copyright (C) 2015 Apple Inc. All Rights Reserved.
See LICENSE.txt for this sample’s licensing information

Abstract:
This file shows how to present an alert as part of an operation.
*/

import UIKit
import PSOperations

open class AlertOperation: PSOperation {
    // MARK: Properties

    fileprivate let alertController = UIAlertController(title: nil, message: nil, preferredStyle: .alert)
    fileprivate let presentationContext: UIViewController?
    
    open var title: String? {
        get {
            return alertController.title
        }

        set {
            alertController.title = newValue
            name = newValue
        }
    }
    
    open var message: String? {
        get {
            return alertController.message
        }
        
        set {
            alertController.message = newValue
        }
    }
    
    // MARK: Initialization
    
    public init(presentationContext: UIViewController? = nil) {
        self.presentationContext = presentationContext ?? UIApplication.shared.keyWindow?.rootViewController

        super.init()
        
        addCondition(AlertPresentation())
        
        /*
            This operation modifies the view controller hierarchy.
            Doing this while other such operations are executing can lead to
            inconsistencies in UIKit. So, let's make them mutally exclusive.
        */
        addCondition(MutuallyExclusive<UIViewController>())
    }
    
    open func addAction(_ title: String, style: UIAlertAction.Style = .default, handler: @escaping (AlertOperation) -> Void = { _ in }) {
        let action = UIAlertAction(title: title, style: style) { [weak self] _ in
            if let strongSelf = self {
                handler(strongSelf)
            }

            self?.finish()
        }
        
        alertController.addAction(action)
    }
    
    override open func execute() {
        guard let presentationContext = presentationContext else {
            finish()

            return
        }

        DispatchQueue.main.async {
            if self.alertController.actions.isEmpty {
                self.addAction("OK")
            }
            
            presentationContext.present(self.alertController, animated: true, completion: nil)
            
            if self.alertController.presentingViewController == nil {
                // there has been an error in presentation, such as this is attempting to present on a tab that isn't visible yet. So finish the operation to prevent other mutually exclusive operations from occuring.
                let error = NSError(code: .executionFailed)
                self.finish([error])
            }
        }
    }
}

/// Creates an `AlertOperation` for an array of errors, appending the `localizedDescription`s of the unique errors to the `message` parameter.
open class AlertErrorOperation: AlertOperation {
    
    public init(presentationContext:UIViewController? = nil, errors:[NSError], title:String?, message:String?) {

        super.init(presentationContext: presentationContext)
        
        self.title = title
        
        var encounteredErrors = [String]()
        let uniqueErrors = errors.compactMap({ (error:NSError) -> NSError? in
            
            let errorID = error.domain + "\(error.code)" + error.localizedDescription
            
            guard !encounteredErrors.contains(errorID) else {
                return nil
            }
            
            encounteredErrors.append(errorID)
            
            return error
        })
        
        let errorDescription = uniqueErrors.reduce("") { (message:String, error:NSError) -> String in
            
            var updated = message
            if !updated.isEmpty {
                updated += "\n"
            }
            
            updated += error.localizedDescription
            
            if let failure = error.localizedFailureReason {
                updated += " \(failure)"
            }
            
            if let recovery = error.localizedRecoverySuggestion {
                updated += " \(recovery)"
            }
            
            return updated
        }
        
        var alertMessage = ""
        if let m = message {
            alertMessage += m + " "
        }
        
        self.message = alertMessage + errorDescription
    }
}
