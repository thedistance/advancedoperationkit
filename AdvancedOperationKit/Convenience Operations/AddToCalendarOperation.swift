//
//  AddToCalendarOperation.swift
//  Pods
//
//  Created by Josh Campion on 15/10/2015.
//
//

import Foundation
import EventKit
//import EventKitUI With iOS 8.1 EventKitUI is not supported in extensions anymore https://developer.apple.com/library/content/releasenotes/General/iOS81APIDiffs/frameworks/EventKitUI.html

import PSOperations

open class AddToCalendarOperation: PSOperation {
    public override init()
    {
        NSLog("Empty implementation")
    }
    
}




/*
open class AddToCalendarOperation: PSOperation, EKEventEditViewDelegate {
    
    open var completion:((_ event:EKEvent?, _ action:EKEventEditViewAction) -> ())?
    
    open var calendarEvent:EKEvent
    
    open var sourceViewController:UIViewController
    open var sourceItem:UIPopoverSourceType
    open var allowsCalendarPreview: Bool
    open var allowsEditing: Bool
    
    open static let SharedEventStore = EKEventStore()
    
    public init(calendarEvent: EKEvent, sourceViewController:UIViewController, sourceItem:UIPopoverSourceType, allowsCalendarPreview:Bool = true, allowsEditing:Bool = true) {
        
        self.calendarEvent = calendarEvent
        
        self.sourceViewController = sourceViewController
        self.sourceItem = sourceItem
        self.allowsCalendarPreview = allowsCalendarPreview
        self.allowsEditing = allowsEditing
        
        super.init()
        
        self.addCondition(Capability(EKEntityType.event))
    }
    
    open func eventViewControllerForEvent(_ calendarEvent:EKEvent) -> EKEventEditViewController {
        
        let calendarVC = EKEventEditViewController()
        calendarVC.event = calendarEvent
        calendarVC.editViewDelegate = self
        calendarVC.eventStore = AddToCalendarOperation.SharedEventStore
        
        return calendarVC
    }
    
    override open func execute() {
        
        let calendarVC = eventViewControllerForEvent(calendarEvent)
        
        // configure for pop up
        switch self.sourceItem {
        case .view(let view):
            calendarVC.popoverPresentationController?.sourceView = view
            calendarVC.popoverPresentationController?.sourceRect = view.bounds
        case .barButton(let barButton):
            calendarVC.popoverPresentationController?.barButtonItem = barButton
        }
        
        DispatchQueue.main.async { () -> Void in
            self.sourceViewController.present(calendarVC, animated: true, completion: nil)
        }
    }
    
    open func eventEditViewController(_ controller: EKEventEditViewController, didCompleteWith action: EKEventEditViewAction) {
        
        DispatchQueue.main.async { () -> Void in
            
            self.completion?(controller.event, action)
            
            self.sourceViewController.dismiss(animated: true, completion: nil)
            self.finish()
        }
    }
    
    override open func finished(_ errors: [NSError]) {
        for error in errors {
            if error.domain == OperationErrorDomain && error.code == OperationErrorCode.conditionFailed,
                let conditionKey = error.userInfo[OperationConditionKey] as? String, conditionKey == Capability<EKEntityType>.name {
                    
                    // failed as wasn't authorized
                    let alertOperation = AlertOperation(presentationContext: sourceViewController)
                    alertOperation.title = "Permission Denied"
                    let privacyReason = (Bundle.main.infoDictionary?["NSCalendarsUsageDescription"] as? String) ?? ""
                    alertOperation.message = "This app does not have access to your device's calendars. You can enable access in Privacy Settings.\n" + privacyReason
                    
                    alertOperation.addAction("Settings", style: .default, handler: { (alertOperation) -> Void in
                        if let appSettings = URL(string: UIApplicationOpenSettingsURLString) {
                            UIApplication.shared.openURL(appSettings)
                        }
                    })
                    
                    alertOperation.addAction("Cancel")
                    
                    self.produceOperation(alertOperation)
            }
        }
    }
}
 
*/
