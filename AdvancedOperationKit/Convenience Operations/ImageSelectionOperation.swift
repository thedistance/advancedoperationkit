//
//  ImageSelectionOperation.swift
//  Pods
//
//  Created by Josh Campion on 17/09/2015.
//
//

import Foundation
import PSOperations

import Photos
import AVKit
import MobileCoreServices

public enum UIPopoverSourceType {
    case barButton(UIBarButtonItem)
    case view(UIView)
}

public enum UIImagePickerResultType {
    case none
    case image(UIImage)
    case movie(URL)
}

public let UIImagePickerTypeImage = kUTTypeImage as String
public let UIImagePickerTypeMovie = kUTTypeMovie as String

open class ImageSelectionOperation: BaseImageSelectionOperation {
    
    open func alertController() -> UIAlertController {
        
        let alert = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        // add the actions
        alert.addAction(UIAlertAction(title: "Photo Library", style: .default, handler: { (action) -> Void in
            
            let photosOperation = PhotosSelectionOperation(sourceViewController: self.sourceViewController,
                sourceItem: self.sourceItem,
                mediaTypes:  self.mediaTypes,
                allowsEditing: self.allowsEditing,
                tint: self.tint)
            photosOperation.completion = self.completion
            
            // create a permissionOperation rather than making it a dependency to show the default UI to the user.
            self.produceOperation(photosOperation)
            
            self.finish()
        }))
        
        if UIImagePickerController.isSourceTypeAvailable(.camera) {
            
            if UIImagePickerController.isSourceTypeAvailable(.camera) {
                
                let takePhoto = mediaTypes.contains(UIImagePickerTypeImage)
                let takeMovie = mediaTypes.contains(UIImagePickerTypeMovie)
                
                let title:String
                switch (takePhoto, takeMovie) {
                case (true, true):
                    title = "Take Photo or Video"
                case (false, true):
                    title = "Take Video"
                default:
                    title = "Take Photo"
                }
                
                alert.addAction(UIAlertAction(title: title,
                    style: .default,
                    handler: { (action) -> Void in
                        
                        let cameraOperation = CameraOperation(sourceViewController: self.sourceViewController,
                            sourceItem: self.sourceItem,
                            mediaTypes:  self.mediaTypes,
                            allowsEditing: self.allowsEditing,
                            tint: self.tint)
                        cameraOperation.completion = self.completion
                        
                        self.produceOperation(cameraOperation)
                        
                        
                        self.finish()
                }))
            }
        }
        
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { (_) -> Void in
            self.completion?(.none, nil)
            self.finish()
        }))
        
        // configure for pop up
        switch self.sourceItem {
        case .view(let view):
            alert.popoverPresentationController?.sourceView = view
            alert.popoverPresentationController?.sourceRect = view.bounds
        case .barButton(let barButton):
            alert.popoverPresentationController?.barButtonItem = barButton
        }
        
        return alert
    }
    
    open override func execute() {
        
        let alert = alertController()
        
        DispatchQueue.main.async { () -> Void in
            self.sourceViewController.present(alert, animated: true, completion: nil)
        }
    }
}

open class BaseImageSelectionOperation: PSOperation, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    open var sourceViewController:UIViewController
    open var sourceItem:UIPopoverSourceType
    open var mediaTypes:[String]
    open var tint:UIColor?
    open var allowsEditing:Bool
    
    open var completion:((_ selectedMedia:UIImagePickerResultType, _ withInfo:[String: AnyObject]?) -> ())?
    
    public init(sourceViewController:UIViewController, sourceItem:UIPopoverSourceType, mediaTypes:[String] = [kUTTypeImage as String], allowsEditing:Bool = false, tint:UIColor? = nil) {
        
        self.sourceViewController = sourceViewController
        self.sourceItem = sourceItem
        self.mediaTypes = mediaTypes
        self.allowsEditing = allowsEditing
        self.tint = tint
        
        super.init()
    }
    
  open func imagePickerControllerOfSourceType(_ sourceType: UIImagePickerController.SourceType) -> UIImagePickerController {
        
        let imagePicker = UIImagePickerController()
        imagePicker.delegate = self
        imagePicker.sourceType = sourceType
        imagePicker.isModalInPopover = true
        imagePicker.mediaTypes = mediaTypes
        imagePicker.allowsEditing = self.allowsEditing
        
        return imagePicker
    }
    
    // MARK: - Image Picker Delegate Methods
    
    public func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        let type = info[UIImagePickerController.InfoKey.mediaType] as? String
        
        var anyInfo: [String: AnyObject] = [:]
        
        for key in info.keys {
            guard let obj = info[key] as? AnyObject else { continue }
            anyInfo[key.rawValue] = obj
        }

        if type == UIImagePickerTypeImage {

            if let selectedImage = (info[UIImagePickerController.InfoKey.editedImage] as? UIImage) ?? (info[UIImagePickerController.InfoKey.originalImage] as? UIImage) {
                completion?(.image(selectedImage), anyInfo)
            } else {
                completion?(.none, anyInfo)
            }

        } else if type == UIImagePickerTypeMovie {

          if let movieURL = info[UIImagePickerController.InfoKey.mediaURL] as? URL {
                completion?(.movie(movieURL), anyInfo)
            } else {
                completion?(.none, anyInfo)
            }

        } else {
            completion?(.none, anyInfo)
        }

        self.sourceViewController.dismiss(animated: true, completion: nil)
        
        finish()
    }
    
    public func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
        let type = info[UIImagePickerController.InfoKey.mediaType.rawValue] as? String
        
        guard let anyInfo =  info as [String : AnyObject]? else {
            print("Error converting \(info)")
            return
        }
        
        if type == UIImagePickerTypeImage {
            
            if let selectedImage = (info[UIImagePickerController.InfoKey.editedImage.rawValue] as? UIImage) ?? (info[UIImagePickerController.InfoKey.originalImage.rawValue] as? UIImage) {
                completion?(.image(selectedImage), anyInfo)
            } else {
                completion?(.none, anyInfo)
            }
            
        } else if type == UIImagePickerTypeMovie {
            
          if let movieURL = info[UIImagePickerController.InfoKey.mediaURL.rawValue] as? URL {
                completion?(.movie(movieURL), anyInfo)
            } else {
                completion?(.none, anyInfo)
            }
            
        } else {
            completion?(.none, anyInfo)
        }
        
        self.sourceViewController.dismiss(animated: true, completion: nil)
        
        finish()
    }
    
    public func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        
        completion?(.none, nil)
        
        self.sourceViewController.dismiss(animated: true, completion:nil)
        
        finish()
    }
}

open class PhotosSelectionOperation: BaseImageSelectionOperation {
    
    override init(sourceViewController: UIViewController, sourceItem: UIPopoverSourceType, mediaTypes: [String], allowsEditing allowingEditing: Bool, tint: UIColor?) {
        super.init(sourceViewController: sourceViewController, sourceItem: sourceItem, mediaTypes: mediaTypes, allowsEditing: allowingEditing, tint: tint)
        self.addCondition(Capability(Photos()))
    }
    
    open override func execute() {
        
        DispatchQueue.main.async {
            let imagePicker = self.imagePickerControllerOfSourceType(.photoLibrary)
            imagePicker.modalPresentationStyle = .popover
            
            switch self.sourceItem {
            case .view(let view):
                imagePicker.popoverPresentationController?.sourceView = view
                imagePicker.popoverPresentationController?.sourceRect = view.bounds
            case .barButton(let barButton):
                imagePicker.popoverPresentationController?.barButtonItem = barButton
            }
            
            if let tintColour = self.tint {
                imagePicker.navigationBar.tintColor = tintColour
            }
            
            self.sourceViewController.present(imagePicker, animated: true, completion: nil)
        }
    }
    
    open override func finished(_ errors: [NSError]) {
        for error in errors {
            if error.domain == OperationErrorDomain && error.code == OperationErrorCode.conditionFailed,
                let conditionKey = error.userInfo[OperationConditionKey] as? String, conditionKey ==  Capability<Photos>.name {
                    // failed as wasn't authorized
                    
                    let alertOperation = AlertOperation(presentationContext: sourceViewController)
                    alertOperation.title = "Permission Denied"
                    let privacyReason = (Bundle.main.infoDictionary?["NSPhotoLibraryUsageDescription"] as? String) ?? ""
                    alertOperation.message = "This app does not have access to your photos or videos. You can enable access in Privacy Settings.\n" + privacyReason
                    
                    alertOperation.addAction("OK")
                    
                    alertOperation.addAction("Settings", style: .default, handler: { (alertOperation) -> Void in
                      if let appSettings = URL(string: UIApplication.openSettingsURLString) {
                            UIApplication.shared.openURL(appSettings)
                        }
                    })
                    
                    self.produceOperation(alertOperation)
            }
        }
    }
}

open class CameraOperation: BaseImageSelectionOperation {
    
    override init(sourceViewController: UIViewController, sourceItem: UIPopoverSourceType, mediaTypes: [String], allowsEditing: Bool, tint: UIColor?) {
        
        super.init(sourceViewController: sourceViewController, sourceItem: sourceItem, mediaTypes: mediaTypes, allowsEditing: allowsEditing, tint: tint)
        self.addCondition(CameraCondition())
    }
    
    open override func execute() {
        
        DispatchQueue.main.async {
            let imagePicker = self.imagePickerControllerOfSourceType(.camera)
            self.sourceViewController.present(imagePicker, animated: true, completion: nil)
        }
    }
    
    open override func finished(_ errors: [NSError]) {
        for error in errors {
            if error.domain == OperationErrorDomain && error.code == OperationErrorCode.conditionFailed,
                let conditionKey = error.userInfo[OperationConditionKey] as? String, conditionKey == CameraCondition.name {
                    
                    // failed as wasn't authorized
                    let alertOperation = AlertOperation(presentationContext: sourceViewController)
                    alertOperation.title = "Permission Denied"
                    let privacyReason = (Bundle.main.infoDictionary?["NSCameraUsageDescription"] as? String) ?? ""
                    alertOperation.message = "This app does not have access to your device's camera. You can enable access in Privacy Settings.\n" + privacyReason
                    
                    alertOperation.addAction("OK")
                    
                    alertOperation.addAction("Settings", style: .default, handler: { (alertOperation) -> Void in
                      if let appSettings = URL(string: UIApplication.openSettingsURLString) {
                            UIApplication.shared.openURL(appSettings)
                        }
                    })
                    
                    self.produceOperation(alertOperation)
            }
        }
    }
}
