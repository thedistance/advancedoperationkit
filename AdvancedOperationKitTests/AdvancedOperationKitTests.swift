//
//  AdvancedOperationKitTests.swift
//  AdvancedOperationKitTests
//
//  Created by Josh Campion on 04/07/2015.
//  Copyright (c) 2015 Josh Campion. All rights reserved.
//

import UIKit
import XCTest
@testable import AdvancedOperationKit

class AdvancedOperationKitOperationTests: AdvancedOperationTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
        
        Operation.verbosity = .All
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testOperationCompletes() {
        
        var counter = 0
        
        let op = Operation()
        op.completionBlock = {
            counter += 1
            print("counting")
        }
        
        operationQueue.addOperation(op)
        
        let expectation = self.expectation(description: "TestQueueCleared")
        
        DispatchQueue.main.async {
            
            sleep(3)
            
            XCTAssertEqual(counter, 1, "Operation completion block not ran.")
            XCTAssertEqual(self.operationQueue.operationCount, 0, "Operation has not been removed from queue.")
            
            expectation.fulfill()
        }
        
        waitForExpectations(timeout: 5.0) { (error) -> Void in
            XCTAssert(error == nil)
        }
    }
}
