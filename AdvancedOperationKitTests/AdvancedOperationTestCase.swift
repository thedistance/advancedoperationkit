//
//  AdvancedOperationTestCase.swift
//  Pods
//
//  Created by Josh Campion
//
//

import XCTest
@testable import AdvancedOperationKit

typealias OperationCompletionClosure = (_ operation: Operation, _ errors:[NSError]) -> ()

class AdvancedOperationTestCase: XCTestCase, OperationQueueDelegate {
    
    var operationExpectations = [String:XCTestExpectation]()
    
    var operationCompletions = [String:OperationCompletionClosure]()
    
    var anyOperationCompletion: OperationCompletionClosure?
    
    override func setUp() {
        super.setUp()
        
        operationExpectations.removeAll()
        operationCompletions.removeAll()
        anyOperationCompletion = nil
    }
    
    lazy var operationQueue: OperationQueue = {
        let queue = OperationQueue()
        queue.delegate = self
        
        return queue
        }()
    
    func registerAndRunOperation(_ operation:Operation, named:String, timeout: TimeInterval = 10.0, handler: XCWaitCompletionHandler? = nil, completion:@escaping OperationCompletionClosure) {
        let expectation = self.expectation(description: named)
        operation.name = named
        
        registerOperation(operation, withExpectation: expectation, completion: completion)
        
        operationQueue.addOperation(operation)
        
        waitForExpectations(timeout: timeout, handler: handler)
    }
    
    func registerOperation(_ operation:Operation, withExpectation expectation:XCTestExpectation, completion:@escaping OperationCompletionClosure) {
        
        if let key = operation.name {
            operationExpectations[key] = expectation
            operationCompletions[key] = completion
        } else {
            assertionFailure("Cannot register an operation with no name.")
        }
    }
    
    func operationQueue(_ operationQueue: OperationQueue, operationDidFinish operation: Operation, withErrors errors: [NSError]) {
        
        print("operation finished: \(operation)")
        
        if let key = operation.name,
            let expectation = operationExpectations[key],
            let completion = operationCompletions[key] {
                
                DispatchQueue.main.async(execute: { () -> Void in
                    expectation.fulfill()
                    completion(operation, errors)
                })
                
                operationCompletions[key] = nil
                operationExpectations[key] = nil
        } else if let completion = anyOperationCompletion {
            DispatchQueue.main.async(execute: { () -> Void in
                completion(operation, errors)
            })
        }
    }
    
}


